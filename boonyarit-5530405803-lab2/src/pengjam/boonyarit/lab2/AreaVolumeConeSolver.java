package pengjam.boonyarit.lab2;

public class AreaVolumeConeSolver {
	public static void main(String args[]) {
		if (args.length != 3) {
			System.err.println("AreaVolumeConeSolver <r> <s> <h>");
			System.exit(0);
		}
		double r  = Double.parseDouble(args[0]),
			   s  = Double.parseDouble(args[1]),
			   h  = Double.parseDouble(args[2]),
			   sa = Math.PI*r*s + Math.PI*Math.pow(r, 2) ,
			   vol = (Math.PI*Math.pow(r, 2)*h)/3;			  
		System.out.println("For cone with r " + r +" s "+ s + " h "+ h );
		System.out.println("Surface area is " + sa + " volume is " + vol );
		
		
	}

}
